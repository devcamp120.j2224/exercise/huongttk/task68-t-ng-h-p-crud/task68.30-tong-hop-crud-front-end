$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/drinks";
  var gTable = $("#drink-table").DataTable({
    "columns": [
      { "data" : "id" },
      { "data" : "maNuocUong" },
      { "data" : "tenNuocUong" },
      { "data" : "donGia" },
      { "data" : "ghiChu" },
      { "data" : "ngayTao" },
      { "data" : "ngayCapNhat" },
      { data: 'action' },
    ],
    columnDefs: [
      {
        targets: 7,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllDrinks();
  })
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    $("#modal-update-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vDrinkData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vDrinkData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewDrink(vDrinkData);
      resetModalAddData();
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vDrinkData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vDrinkData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateDrinkById(gSelectedId, vDrinkData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteDrinkById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllDrinks() { // gọi api lấy list all dữ liệu
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListDrinkToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListDrinkToTable(paramListDrink) { // đổ list dữ liệu vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListDrink);
    gTable.draw();
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vDrinkData = {
      maNuocUong: $("#input-maNuocUong-modal-add-data").val().trim(),
      tenNuocUong: $("#input-tenNuocUong-modal-add-data").val().trim(),
      donGia: $("#input-donGia-modal-add-data").val().trim(),
      ghiChu: $("#input-ghiChu-modal-add-data").val().trim(),
    };			
    console.log("Thông tin Drink đọc được là:");
    console.log(vDrinkData);
    return vDrinkData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vDrinkData = {
      maNuocUong: $("#input-maNuocUong-modal-update-data").val().trim(),
      tenNuocUong: $("#input-tenNuocUong-modal-update-data").val().trim(),
      donGia: $("#input-donGia-modal-update-data").val().trim(),
      ghiChu: $("#input-ghiChu-modal-update-data").val().trim(),
    };			
    console.log("Thông tin Drink đọc được là:");
    console.log(vDrinkData);
    return vDrinkData
  }
  function loadDataToModalUpdateData(paramDrink) { // đổ dữ liệu vào modal update data
    "use strict";
    $("#input-maNuocUong-modal-update-data").val(paramDrink.maNuocUong);
    $("#input-tenNuocUong-modal-update-data").val(paramDrink.tenNuocUong);
    $("#input-donGia-modal-update-data").val(paramDrink.donGia);
    $("#input-ghiChu-modal-update-data").val(paramDrink.ghiChu);
    $('#input-ngayTao-modal-update-data').val(formatNgayThangNam(paramDrink.ngayTao));
    $('#input-ngayCapNhat-modal-update-data').val(formatNgayThangNam(paramDrink.ngayCapNhat));
  }
  function formatNgayThangNam(date) { // hàm dùng để format ngày tháng
    var vArray = date.split("-"),
      day = vArray[0],
      month = vArray[1],
      year = vArray[2];
    return [year, month, day].join('-');
  }
  function validateData(paramDrinkData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramDrinkData.maNuocUong == "") {
      alert("Chưa nhập Mã Nước uống")
    }
    else if (checkMaNuocUongDaTonTai(paramDrinkData.maNuocUong, gSelectedId)) {
      alert("Đã tồn tại Mã Nước uống này");
    }
    else if (paramDrinkData.tenNuocUong == "") {
      alert("Chưa nhập Tên Nước uống")
    }
    else if (paramDrinkData.donGia == "") {
      alert("Chưa nhập Đơn giá")
    }
    else if (paramDrinkData.donGia < 1000 || paramDrinkData.donGia > 500000 || Number.isInteger(Number(paramDrinkData.donGia)) == false) {
      alert("Đơn giá phải là số nguyên dương từ 1.000 đến 500.000")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function checkMaNuocUongDaTonTai(paramMaNuocUong, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].maNuocUong == paramMaNuocUong && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
  function resetModalAddData() {
    $("#input-maNuocUong-modal-add-data").val("");
    $("#input-tenNuocUong-modal-add-data").val("");
    $("#input-donGia-modal-add-data").val("");
    $("#input-ghiChu-modal-add-data").val("");
  }
  function callAjaxApiAddNewDrink(paramDrinkData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramDrinkData),
      success: function(res) {
        callAjaxApiGetAllDrinks();
        console.log("Tạo Drink mới thành công. Response là:");
        console.log(res);
        alert("Tạo Drink mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err.response);
        alert("Tạo Drink mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateDrinkById(paramId, paramDrinkData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramDrinkData),
      success: function(res) {
        callAjaxApiGetAllDrinks();
        console.log("Update thành công cho Drink có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho Drink có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteDrinkById(paramDrinkId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramDrinkId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllDrinks();
        console.log("Xóa Drink có id " + paramDrinkId + " thành công. Response là:");
        console.log(res);
        alert("Xóa Drink có id " + paramDrinkId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Drink có id " + paramDrinkId + " không thành công!");
      }
    })
  }
})
