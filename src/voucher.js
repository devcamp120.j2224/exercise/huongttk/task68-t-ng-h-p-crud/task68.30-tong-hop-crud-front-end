$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/vouchers";
  var gTable = $("#voucher-table").DataTable({
    "columns": [
      { data: 'id' },
      { data: 'maVoucher' },
      { data: 'phanTramGiamGia' },
      { data: 'ghiChu' },
      { data: 'ngayTao' },
      { data: 'ngayCapNhat' },
      { data: 'action' },
    ],
    columnDefs: [
      {
        targets: 6,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllVouchers();
  })
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    $("#modal-update-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vVoucherData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vVoucherData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewVoucher(vVoucherData);
      resetModalAddData();
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vVoucherData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vVoucherData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateVoucherById(gSelectedId, vVoucherData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteVoucherById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllVouchers() { // gọi api lấy list all dữ liệu
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListVoucherToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListVoucherToTable(paramListVoucher) { // đổ list dữ liệu vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListVoucher);
    gTable.draw();
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vVoucherData = {
      maVoucher: $('#input-voucher-code-modal-add-data').val().trim(),
      phanTramGiamGia: $('#input-discount-modal-add-data').val().trim(),
      ghiChu: $('#input-note-modal-add-data').val().trim(),
    };			
    console.log("Thông tin Voucher đọc được là:");
    console.log(vVoucherData);
    return vVoucherData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vVoucherData = {
      maVoucher: $('#input-voucher-code-modal-update-data').val().trim(),
      phanTramGiamGia: $('#input-discount-modal-update-data').val().trim(),
      ghiChu: $('#input-note-modal-update-data').val().trim(),
    };			
    console.log("Thông tin Voucher đọc được là:");
    console.log(vVoucherData);
    return vVoucherData
  }
  function loadDataToModalUpdateData(paramVoucher) { // đổ dữ liệu vào modal update data
    "use strict";
    $('#input-voucher-code-modal-update-data').val(paramVoucher.maVoucher);
    $('#input-discount-modal-update-data').val(paramVoucher.phanTramGiamGia);
    $('#input-note-modal-update-data').val(paramVoucher.ghiChu);
    $('#input-ngayTao-modal-update-data').val(formatNgayThangNam(paramVoucher.ngayTao));
    $('#input-ngayCapNhat-modal-update-data').val(formatNgayThangNam(paramVoucher.ngayCapNhat));
  }
  function formatNgayThangNam(date) { // hàm dùng để format ngày tháng
    var vArray = date.split("-"),
      day = vArray[0],
      month = vArray[1],
      year = vArray[2];
    return [year, month, day].join('-');
  }
  function validateData(paramVoucherData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramVoucherData.maVoucher == "") {
      alert("Chưa nhập Mã Voucher")
    }
    else if (checkMaVoucherDaTonTai(paramVoucherData.maVoucher, gSelectedId)) {
      alert("Đã tồn tại Mã Voucher này");
    }
    else if (paramVoucherData.phanTramGiamGia == "") {
      alert("Chưa nhập Phần trăn giảm giá")
    }
    else if (paramVoucherData.phanTramGiamGia < 0 || paramVoucherData.phanTramGiamGia > 99 || isNaN(Number(paramVoucherData.phanTramGiamGia))) {
      alert("Phần trăn giảm giá phải là số nguyên dương nhỏ hơn 100")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function checkMaVoucherDaTonTai(paramMaVoucher, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].maVoucher == paramMaVoucher && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
  function resetModalAddData() {
    $('#input-voucher-code-modal-add-data').val('');
    $('#input-discount-modal-add-data').val('');
    $('#input-note-modal-add-data').val('');
  }
  function callAjaxApiAddNewVoucher(paramVoucherData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramVoucherData),
      success: function(res) {
        callAjaxApiGetAllVouchers();
        console.log("Tạo Voucher mới thành công. Response là:");
        console.log(res);
        alert("Tạo Voucher mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err.response);
        alert("Tạo Voucher mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateVoucherById(paramId, paramVoucherData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramVoucherData),
      success: function(res) {
        callAjaxApiGetAllVouchers();
        console.log("Update thành công cho Voucher có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho Voucher có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteVoucherById(paramVoucherId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramVoucherId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllVouchers();
        console.log("Xóa Voucher có id " + paramVoucherId + " thành công. Response là:");
        console.log(res);
        alert("Xóa Voucher có id " + paramVoucherId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Voucher có id " + paramVoucherId + " không thành công!");
      }
    })
  }
})
