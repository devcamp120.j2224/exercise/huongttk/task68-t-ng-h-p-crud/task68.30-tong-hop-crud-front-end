$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/regions";
  var gTable = $("#region-table").DataTable({
    "columns": [
      { "data" : "id" },
      { "data" : "regionCode" },
      { "data" : "regionName" },
      { "data" : "countryName" },
      { "data": 'action' },
    ],
    columnDefs: [
      {
        targets: 4,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllRegions();
    callAjaxApiGetAllCountry();
  })
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    $("#modal-update-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vRegionData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vRegionData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewRegion(vRegionData);
      resetModalAddData();
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vRegionData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vRegionData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateRegionById(gSelectedId, vRegionData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteRegionById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllRegions() { // gọi api lấy list all dữ liệu region
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListRegionToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListRegionToTable(paramListRegion) { // đổ list dữ liệu region vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListRegion);
    gTable.draw();
  }
  function callAjaxApiGetAllCountry() { // gọi api lấy list all dữ liệu country
    $.ajax({
      url: "http://localhost:8080/countries",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadListCountryToSelect(res);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListCountryToSelect(paramListCountry) { // đổ list dữ liệu country vào các select
    "use strict";
    for (let bCountry of paramListCountry) {
      $(".select-country").each(function() {
        $(this).append($("<option>").val(bCountry.id).html(bCountry.countryName));
      })
    };
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vRegionData = {
      countryId: $("#select-country-modal-add-data").val().trim(),
      regionCode: $("#input-regionCode-modal-add-data").val().trim(),
      regionName: $("#input-regionName-modal-add-data").val().trim(),
    };			
    console.log("Thông tin Region đọc được là:");
    console.log(vRegionData);
    return vRegionData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vRegionData = {
      regionCode: $("#input-regionCode-modal-update-data").val().trim(),
      regionName: $("#input-regionName-modal-update-data").val().trim(),
    };			
    console.log("Thông tin Region đọc được là:");
    console.log(vRegionData);
    return vRegionData
  }
  function loadDataToModalUpdateData(paramRegion) { // đổ dữ liệu vào modal update data
    "use strict";
    $("#select-country-modal-update-data").val(paramRegion.countryId),
    $("#input-regionCode-modal-update-data").val(paramRegion.regionCode);
    $("#input-regionName-modal-update-data").val(paramRegion.regionName);
  }
  function validateData(paramRegionData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramRegionData.countryId == "") {
      alert("Chưa chọn Country")
    }
    else if (paramRegionData.regionCode == "") {
      alert("Chưa nhập Mã Region")
    }
    else if (checkRegionCodeDaTonTai(paramRegionData.regionCode, gSelectedId)) {
      alert("Đã tồn tại Mã Region này");
    }
    else if (paramRegionData.regionName == "") {
      alert("Chưa nhập Tên Region")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function checkRegionCodeDaTonTai(paramRegionCode, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].regionCode == paramRegionCode && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
  function resetModalAddData() {
    $("#select-country-modal-add-data").val("");
    $("#input-regionCode-modal-add-data").val("");
    $("#input-regionName-modal-add-data").val("");
  }
  function callAjaxApiAddNewRegion(paramRegionData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/country/" + paramRegionData.countryId,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramRegionData),
      success: function(res) {
        callAjaxApiGetAllRegions();
        console.log("Tạo Region mới thành công. Response là:");
        console.log(res);
        alert("Tạo Region mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err.response);
        alert("Tạo Region mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateRegionById(paramId, paramRegionData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramRegionData),
      success: function(res) {
        callAjaxApiGetAllRegions();
        console.log("Update thành công cho Region có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho Region có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteRegionById(paramRegionId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramRegionId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllRegions();
        console.log("Xóa Region có id " + paramRegionId + " thành công. Response là:");
        console.log(res);
        alert("Xóa Region có id " + paramRegionId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Region có id " + paramRegionId + " không thành công!");
      }
    })
  }
})
