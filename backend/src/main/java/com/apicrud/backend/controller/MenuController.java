package com.apicrud.backend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.apicrud.backend.model.CMenu;
import com.apicrud.backend.repository.IMenuRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/menus")
public class MenuController {
    @Autowired
    private IMenuRepository iMenuRepository;

    @GetMapping("")
    public ResponseEntity<Object> getAllMenus() {
        try {
            List<CMenu> listMenu = new ArrayList<CMenu>();
            iMenuRepository.findAll().forEach(listMenu::add);
            return new ResponseEntity<>(listMenu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to get all Menus" + 
                e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getMenuById(@PathVariable int id) {
        Optional<CMenu> existedMenu = iMenuRepository.findById(id);
        if(existedMenu.isPresent()) {
            try {
                return new ResponseEntity<>(existedMenu.get(), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>("Failed to get Menu" + 
                    e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<Object> createMenu(@RequestBody CMenu pMenu) {
        try {
            CMenu newMenu = new CMenu();
            newMenu.setKichCo(pMenu.getKichCo());
            newMenu.setDuongKinh(pMenu.getDuongKinh());
            newMenu.setSuon(pMenu.getSuon());
            newMenu.setSalad(pMenu.getSalad());
            newMenu.setSoLuongNuoc(pMenu.getSoLuongNuoc());
            newMenu.setThanhTien(pMenu.getThanhTien());
            return new ResponseEntity<>(iMenuRepository.save(newMenu), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to create Menu" + 
                e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateMenuById(@PathVariable int id, @RequestBody CMenu pMenu) {
        Optional<CMenu> existedMenu = iMenuRepository.findById(id);
        if(existedMenu.isPresent()) {
            try {
                CMenu updatedMenu = existedMenu.get();
                updatedMenu.setKichCo(pMenu.getKichCo());
                updatedMenu.setDuongKinh(pMenu.getDuongKinh());
                updatedMenu.setSuon(pMenu.getSuon());
                updatedMenu.setSalad(pMenu.getSalad());
                updatedMenu.setSoLuongNuoc(pMenu.getSoLuongNuoc());
                updatedMenu.setThanhTien(pMenu.getThanhTien());
                return new ResponseEntity<>(iMenuRepository.save(updatedMenu), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>("Failed to update Menu" + 
                    e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteMenuById(@PathVariable int id) {
        Optional<CMenu> existedMenu = iMenuRepository.findById(id);
        if(existedMenu.isPresent()) {
            try {
                iMenuRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>("Failed to delete Menu" + 
                    e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
        }
    }
}
