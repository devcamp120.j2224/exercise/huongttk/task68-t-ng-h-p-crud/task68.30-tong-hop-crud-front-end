package com.apicrud.backend.controller;

import com.apicrud.backend.model.CCountry;
import com.apicrud.backend.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/countries")
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping("")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Object getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
			.body("Failed to Create Country: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			try {
				CCountry newCountry = countryData.get();
				newCountry.setCountryName(cCountry.getCountryName());
				newCountry.setCountryCode(cCountry.getCountryCode());
				CCountry savedCountry = countryRepository.save(newCountry);
				return new ResponseEntity<>(savedCountry, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
				.body("Failed to Update Country: "+e.getCause().getCause().getMessage());
			}
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/count")
	public long countCountry() {
		return countryRepository.count();
	}
	
	@GetMapping("/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}

	@GetMapping("/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@GetMapping("/code")
	public CCountry getCountryByCountryCode(@RequestParam String countryCode) {
		return countryRepository.findByCountryCode(countryCode);
	}
	
	@GetMapping("/name")
	public List<CCountry> getCountryByCountryName(@RequestParam String countryName) {
		return countryRepository.findByCountryName(countryName);
	}
	
	@GetMapping("/region-code")
	public CCountry getCountryByRegionCode(@RequestParam String regionCode) {
		return countryRepository.findByRegionsRegionCode(regionCode);
	}
	
	@GetMapping("/region-name")
	public List<CCountry> getCountryByRegionName(@RequestParam String regionName) {
		return countryRepository.findByRegionsRegionName(regionName);
	}		
}
