package com.apicrud.backend.repository;

import com.apicrud.backend.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
