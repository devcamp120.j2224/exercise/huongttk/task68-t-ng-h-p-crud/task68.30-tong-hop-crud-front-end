package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicrud.backend.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
}
