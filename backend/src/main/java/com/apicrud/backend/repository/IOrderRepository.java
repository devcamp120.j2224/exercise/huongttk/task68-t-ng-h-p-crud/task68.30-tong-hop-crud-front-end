package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicrud.backend.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
}
